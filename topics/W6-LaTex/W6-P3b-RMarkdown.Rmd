---
title: "Tea Time"
author: "Tim Peshek"
date: "July 14, 2016"
output: pdf_document
---

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r}
summary(cars)
```

*italics*

**BoldFace**

**You can also embed plots, for example:**


```{r, echo=FALSE}
plot(cars)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

******

** How to Add Bullets **

* Item 1
* Item 2
    + item3
    + item4
    
Dont forget the space between the bullet char and the items and for subitems add TWO (2) tabs

1. Item 1
2. Item 2
3. Item 3
    + Item 3a
    + Item 3b

******

There were `r nrow(cars)` cars studied

http://example.com

[example!](http://example.com)

Blockquotes

A friend once said:

> It's always better to give
> than to receive.

```
This text is displayed verbatim / preformatted
```


We defined the `add` function to
compute the sum of two numbers.
LaTeX Equations



LaTeX Equations

Inline equation:  $\frac{1}{2} CV^2$

Display equation:

$$ \frac{1}{2} CV^2 $$

Horizontal Rule / Page Break

Three or more asterisks or dashes:

******

------

Tables

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell





superscript^2^

~~strikethrough~~

[//]: (comment here! )

********

# Graphics

## Inserting image from website

![](http://feelgrafix.com/data/american-flag/american-flag-12.jpg)


