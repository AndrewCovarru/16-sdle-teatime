---
title: "CWRU DSCI351-451: Practicum: R Basics"
author: "Prof.:Roger French, TA:Yang Hu"
date: "January 22, 2015"
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

<!--
# Script Name: 1501cwru-dsci-351-451-R-basics.Rmd
# Purpose: This is a a quick intro to R basics  
# Authors: Roger H. French
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########
# Latest Changelog Entires:
# v0.00.02 - 1501cwru-dsci-351-451-R-basics.Rmd - RF building on Jenny Bryan's intro
##########

# Rmd code goes below the comment marker!
-->
## Simple Rmd rules

- Rcode in Rmd, delineated by three ticks{r}

```{r,echo=TRUE,message=TRUE}
options("digits"=5)
options("digits.secs"=3)
```
 
```{r}
help(c)
```

- Code in Rmd is delineated 
-- three ticks for code blocks
-- one tick for inline code

tick is a back tilted apostrophe
Not a apostrophe!
 
### Commenting in the Rmd body

- To comment in the Rmd body, use html comment form
  < ! - - Comment - - >  
  - but with no spaces between the characters
<!-- regular html comment --> 
''' 
>- i.e.' <!-- ' starts a comment block  
>- ''' --> ''' ends a comment block  

<!--
  So this is a comment block  
  
   In the YAML header its ##    (a bit confusing since this is a 2nd level header in the rest of the file)
         In the rest of the file its <!-- regular html comment -->
-->

### Inserting Figs, 2 ways.



  >- Essential to use relative file paths
  >- Essential to use Posix compatible paths

# Basics of working with R 

At the command line 

And RStudio goodies

## Launch RStudio/R.

Notice the default panes:

- Console (entire left)
- Environment/History (tabbed in upper right)
- Files/Plots/Packages/Help (tabbed in lower right)
>- FYI: you can change the default location of the panes, 
- among many other things: Customizing RStudio.

## Go into the R Console, 

Where we interact with the live R process.

Make an "assignment"" and 

- then inspect the object you just created.

All R statements where you create objects – “assignments” – have this form:
```{r}
x <- 3 * 4
x
```
What you should see "## [1] 12"

# Object (Variable) Assignments

### Assignments are done as 

- objectName <- value
- and in my head I hear, e.g., “x gets 12”.

### You will make lots of assignments 

- And the operator <- is a pain to type. 
- But don’t be lazy and use =, although it would work, 
- because it will just sow confusion later. 
- Instead, utilize RStudio’s keyboard shortcut: Alt + - (the minus sign).

### Notice that RStudio automagically surrounds <- with spaces, 
  which demonstrates a useful code formatting practice. 

> Code is miserable to read on a good day. 
>  Give your eyes a break and use spaces.

### Object names cannot start with a digit

- and cannot contain certain other characters 
- such as a comma or a space. 

### Adopt a convention for demarcating words in names.

- One example is  use_snake_case
- Others.people.use.periods
- evenOthersUseCamelCase

>- [What does Google R Style Guide say](https://google-styleguide.googlecode.com/svn/trunk/Rguide.xml)
>- [What does Hadley Wickham R Style Guide say](http://hadley.github.io/stat405/r-style-guide.html)

- Who is Hadley Wickhmam?
- Why NZ?

#### Make another assignment, and inspecting assignments

this_is_a_really_long_name <- 2.5 

```{r}
this_is_a_really_long_name <- 2.5 
```
>- note back tick, not apostrphe

- shows up in editor window

#### To inspect this, try out RStudio’s completion facility: 

- type the first few characters, press TAB, 
- add characters until you disambiguate, 
- then press return.

### RStudio offers many handy keyboard shortcuts. 

- Also, Alt+Shift+K brings up a keyboard shortcut reference card.

### RStudio also allows on the fly Knitting

> Cntrl Shift K

### Make another assignment

```{r}
jenny_rocks  <- 2^3
```
Let’s try to inspect:

- Do this in your console window

jennyrocks  
## Error in eval(expr, envir, enclos): object 'jennyrocks' not found

jeny_rocks  
## Error in eval(expr, envir, enclos): object 'jeny_rocks' not found

### Implicit contract with the computer / scripting language: 

Computer will do tedious computation for you. 
In return, you will be completely precise in your instructions. 

- Typos matter. 
- Case matters. 
- Get better at typing.

# R has a mind-blowing collection of built-in functions 

-that are accessed like so

##functionName(arg1 = val1, arg2 = val2, and so on)

## Functions  

- Come from base R, 
- Or from R packages
- All the > 7000 R packages are in CRAN
[CRAN = The Comprehensive R Archive Network](http://cran.case.edu/)

>- And you'll write your own functions  

- from your own scripts

### Let’s try using seq() 

- seq() makes regular sequences of numbers and, 

While we’re at it, 

- demo more helpful features of RStudio.

### Type se and hit TAB. 

####A pop up shows you possible completions. 

- Specify seq() by typing more to disambiguate 
- or using the up/down arrows to select. 

#### Notice the floating tool-tip-type help that pops up, 

- reminding you of a function’s arguments. 
- If you want even more help, 
- press F1 as directed 
- to get the full documentation in the help tab of the lower right pane. 

#### Now open the parentheses and 

- notice the automatic addition of the closing parenthesis and 
- the placement of cursor in the middle. 
- Type the arguments 1, 10 and hit return. 

#### RStudio also exits the parenthetical expression for you. 

# IDEs are great.

# Links and License 
 
http://www.r-project.org 

http://rmarkdown.rstudio.com/  

http://stat545-ubc.github.io/block002_hello-r-workspace-wd-project.html

This work is licensed under the CC BY-NC 3.0 Creative Commons License.

<!--
# Keep a complete change log history at bottom of file.
# Complete Change Log History
# v0.00.01 - 1405-07 - Roger French made this markdown from Jenny Bryan's start
##########
Here are standard YAML Headers for Rmarkdown/Markdown/PanDoc
  1. Beamer slides and PDF Report, html
  2. html and PDF report as full long pages
  3.ioSlides slides and PDF report. ioSlides is buggy, not well supported, but looks nice. 

---
title: "1501-DSCI351-451-R-basics.Rmd"
author: Roger French
date: January 22, 2015
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  ioslides_presentation:
    toc: true
    self_contained: true
    smaller: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

-->